import {combineReducers} from 'redux';
import loans from './model/loans/loansReducer';

export default combineReducers({
    loans,
})
