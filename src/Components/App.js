import React, {Component} from 'react'
import AverageLoans from './AverageLoans';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class App extends Component {
    render() {
        return <MuiThemeProvider>
            <AverageLoans />
        </MuiThemeProvider>
    }
}
