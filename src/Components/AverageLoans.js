import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getAllLoans} from '../model/loans/loansActions'
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress'
import config from '../config';
import RatingDetail from './RatingDetail';

class AverageLoans extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRating: null,
        };
    }

    handleGetLoans(rating) {
        this.setState({
            activeRating: rating,
        });
        this.props.getAllLoans(rating);
    }

    render() {
        const {isLoading, isError, aggregated} = this.props;
        const {activeRating} = this.state;
        const showDetail = !isLoading && !isError && aggregated[activeRating] && aggregated[activeRating].average;

        return <div className="container">
            <h1>Average Loans</h1>

            {config.loansRatings.map(rating =>
                <RaisedButton
                    label={rating}
                    key={rating}
                    onClick={this.handleGetLoans.bind(this, rating)}
                    className="rating-button"
                    primary={activeRating === rating}
                />
            )}

            {isLoading &&
                <div className="loader">
                    <CircularProgress />
                </div>
            }

            {showDetail &&
                <RatingDetail
                    activeRating={activeRating}
                    data={aggregated[activeRating]}
                />
            }

            {isError &&
                <div className="error">
                    <p>Sorry, Something Went Wrong. Please try again later.</p>
                </div>
            }
        </div>
    }
}

export default connect(state => ({
    isLoading: state['loans'].isLoading,
    isError: state['loans'].isError,
    aggregated: state['loans'].aggregated,
}), {getAllLoans})(AverageLoans)
