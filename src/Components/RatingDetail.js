import React from 'react';

function RatingDetail(props) {
    return (
        <div className="rating-detail">
            <p className="message">Average Loan amount of <strong>"{props.activeRating}"</strong> rating is <strong>{props.data.average}</strong></p>
            <p className="small-info">This was counted from <strong>{props.data.total}</strong> Loans</p>
        </div>
    );
};

export default RatingDetail
