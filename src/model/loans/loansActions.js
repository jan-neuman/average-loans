import config from '../../config';

export const SET_LOADING = 'SET_LOADING';
export const RESET_LOADING = 'RESET_LOADING';
export const SET_ERROR = 'SET_ERROR';
export const SET_LOANS = 'SET_LOANS';

const reduceLoans = (loans) => {
    return loans.reduce((accumulator, currentValue) => {
        return accumulator + currentValue.amount;
    }, 0);
};

function getLoans(count, rating) {
    return (dispatch, getState, {fetch}) => {
        const queryParam = encodeURIComponent(`["${rating}"]`);
        return fetch.fetchJson(`${config.apiUrl}/loans/marketplace?rating__in=${queryParam}&fields=amount`, {
            headers: {
                'X-Size': count,
            },
        })
            .then(response => {
                const reduced = reduceLoans(response);
                const aggregated = {};

                aggregated[rating] = {
                    reduce: reduced,
                    average: parseInt(reduced / count, 10),
                    total: count,
                };

                dispatch({type: SET_LOANS, aggregated});
            });
    };
}

function getLoansCount(rating) {
    return (dispatch, getState, {fetch}) => {
        const queryParam = encodeURIComponent(`["${rating}"]`);
        return fetch.fetchHeaders(`${config.apiUrl}/loans/marketplace?rating__in=${queryParam}&fields=amount`)
            .then(response => {
                if (response['x-total'] == null) {
                    return dispatch({type: SET_ERROR});
                }

                const aggregated = {};
                aggregated[rating] = {
                    total: parseInt(response['x-total'], 10),
                };

                dispatch({type: SET_LOANS, aggregated});
            });
    };
}

export function getAllLoans(rating) {
    return (dispatch, getState, {fetch}) => {
        if (getState().loans.aggregated[rating].total) {
            return;
        }

        dispatch({type: SET_LOADING});

        return dispatch(getLoansCount(rating))
            .then(() => {
                const count = getState().loans.aggregated[rating].total;

                return dispatch(getLoans(count, rating));
            })
            .then(() => {
                return dispatch({type: RESET_LOADING});
            })
            .catch(() => {
                return dispatch({type: SET_ERROR});
            });
    }
}
