import * as actions from './loansActions';
import config from "../../config";
import createReducer from '../../createReducer';

const aggregated = {};

config.loansRatings.forEach(rating => {
    aggregated[rating] = {};
});

const initData = {
    isLoading: false,
    isError: false,
    aggregated: aggregated,
};

function setLoading(state) {
    return {
        ...state,
        isLoading: true,
    };
}

function resetLoading(state) {
    return {
        ...state,
        isLoading: false,
    };
}

function setError(state) {
    return {
        ...state,
        isLoading: false,
        isError: true,
    };
}

function setLoans(state, {aggregated}) {
    return {
        ...state,
        aggregated: Object.assign({}, state.aggregated, aggregated),
    };
}

export default createReducer(initData, {
    [actions.SET_LOADING]: setLoading,
    [actions.RESET_LOADING]: resetLoading,
    [actions.SET_ERROR]: setError,
    [actions.SET_LOANS]: setLoans,
})
