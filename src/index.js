import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import services from './services';
import {createStore, applyMiddleware} from 'redux';

import reducer from './reducer';

import App from './Components/App';
import './style.scss';

const store = createStore(
    reducer,
    applyMiddleware(thunk.withExtraArgument(services))
);

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);
