export class FetchError extends Error {
    constructor(statusText, response) {
        super(statusText);
        this.response = response;
    };
}

const checkStatus = (response) => {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    throw new FetchError(response.statusText, response);
};

const parseJson = (response) => {
    return response.json();
};

const fetchJson = (path, options) => {
    return fetch(path, options)
        .then(checkStatus)
        .then(parseJson);
};

const fetchHeaders = (path, options) => {
    return fetch(path, {...options, method: 'head'})
        .then(response => {
            const headersResponse = {};
            response.headers.forEach((val, key) => {
                headersResponse[key] = val;
            });
            return headersResponse;
        });
}

export {
    fetchHeaders,
    fetchJson,
}
